//
//  NewsFeedTableViewController.m
//  DCCNews
//
//  Created by Douglas on 21/07/14.
//  Copyright (c) 2014 Douglas. All rights reserved.
//

#import "NewsFeedTableViewController.h"

@interface NewsFeedTableViewController ()

@end

@implementation NewsFeedTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (CGFloat)textViewHeightForAttributedText:(NSAttributedString *)text andWidth:(CGFloat)width
{
    UITextView *textView = [[UITextView alloc] init];
    [textView setAttributedText:text];
    CGSize size = [textView sizeThatFits:CGSizeMake(width, FLT_MAX)];
    return size.height;
}

- (void)viewDidAppear:(BOOL)animated{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    
//    NSDictionary *userInfo = [defaults dictionaryForKey:@"userInfo"];
//    
//    if (!userInfo) {
//        [self performSegueWithIdentifier:@"login" sender:self];
//    }
//    else{
//        
//        if (!webServiceController) {
//            webServiceController = [[WebServiceHandler alloc] init];
//        }
//        
//        [self.userBarButton setTitle:[userInfo objectForKey:@"username"]];
//        
//        self.newsArray = [webServiceController sendDataToWebServiceWithData:nil service:@"tweets" andHTTPMethod:@"GET"];
//        
//        NSLog(@"Twwss %@", self.newsArray);
//        
//        for (NSDictionary *news in self.newsArray) {
//            NSString *serviceParameter = [NSString stringWithFormat:@"users/%@", [news objectForKey:@"user_id"]];
//            NSDictionary *user = [webServiceController sendDataToWebServiceWithData:nil service:serviceParameter andHTTPMethod:@"GET"];
//            
//            NSLog(@"user: %@", user);
//            [self.usersArray addObject:user];
//        }
//        
//        [self.tableView reloadData];
//    }
    
}

- (void)viewWillAppear:(BOOL)animated{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSDictionary *userInfo = [defaults dictionaryForKey:@"userInfo"];
    
    if (!userInfo) {
        [self performSegueWithIdentifier:@"login" sender:self];
    }
    else{
        //[self loadTableDataWithUserInfo:userInfo];
        [NSThread detachNewThreadSelector:@selector(loadTableDataWithUserInfo:) toTarget:self withObject:userInfo];

    }

    
    
}

- (void)loadTableDataWithUserInfo:(NSDictionary *)userInfo{
    
    if (!webServiceController) {
        webServiceController = [[WebServiceHandler alloc] init];
    }
    
    [self.userBarButton setTitle:[userInfo objectForKey:@"username"]];
    
    self.newsArray = [webServiceController sendDataToWebServiceWithData:nil service:@"tweets" andHTTPMethod:@"GET"];
    
    NSLog(@"Twwss %@", self.newsArray);
    
    for (NSDictionary *news in self.newsArray) {
        
//        NSString *text = [news objectForKey:@"text"];
//        
//        UITextView *tempTV = [[UITextView alloc] initWithFrame:CGRectMake(45, 0, 250, 44)];
//        tempTV.text = text;
//        [tempTV performSelectorOnMainThread:@selector(setText:) withObject:text waitUntilDone:NO];
//        
//        CGFloat height = [self textViewHeightForAttributedText:tempTV.attributedText andWidth:tempTV.frame.size.width];
//        
//        [self.heighsArray addObject:[NSNumber numberWithFloat:height]];
        
        NSString *serviceParameter = [NSString stringWithFormat:@"users/%@", [news objectForKey:@"user_id"]];
        NSDictionary *user = [webServiceController sendDataToWebServiceWithData:nil service:serviceParameter andHTTPMethod:@"GET"];
        
        NSLog(@"user: %@", user);
        [self.usersArray addObject:user];
    }
    
    
    
    [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.usersArray = [[NSMutableArray alloc] init];
    self.heighsArray = [[NSMutableArray alloc] init];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    return [self.newsArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"newsCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSInteger row = [indexPath row];
    
    NSDictionary *news = [self.newsArray objectAtIndex:row];
    NSDictionary *user = [self.usersArray objectAtIndex:row];
    
    UITextView *textLabel = (UITextView *)[cell viewWithTag:100];
    UILabel *nameLabel = (UILabel *)[cell viewWithTag:101];
    UILabel *dateLabel = (UILabel *)[cell viewWithTag:102];
    
    textLabel.text = [news objectForKey:@"text"];
    
    
    nameLabel.text = [user objectForKey:@"name"];
    dateLabel.text = [news objectForKey:@"updated_at"];
    
    //[textLabel sizeToFit];
    [nameLabel sizeToFit];
    [dateLabel sizeToFit];
    
    CGFloat height = [self textViewHeightForAttributedText:textLabel.attributedText andWidth:textLabel.frame.size.width];
    
    NSLog(@"height: %f", height);
    
    [textLabel setFrame:CGRectMake(textLabel.frame.origin.x, nameLabel.frame.size.height + 20, textLabel.frame.size.width, height)];
    
    
    return cell;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//      // NSLog(@"depois");
//    NSInteger row = indexPath.row;
////    
////    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
////    
////    UITextView *textView = (UITextView *)[cell viewWithTag:100];
////    
//    NSDictionary *news = [self.newsArray objectAtIndex:row];
//    
//    NSString *text = [news objectForKey:@"text"];
//    
//    NSLog(@"text: %@", text);
//    
//    
//    UITextView *tempTV = [[UITextView alloc] initWithFrame:CGRectMake(45, 0, 250, 44)];
//    tempTV.text = text;
//    
//    CGFloat height = [self textViewHeightForAttributedText:tempTV.attributedText andWidth:tempTV.frame.size.width];
//    
//    NSLog(@"height: %f", height);
//    
////    [tempTV setFrame:CGRectMake(tempTV.frame.origin.x, tempTV.frame.origin.y, tempTV.frame.size.width, height)];
//    
////    NSAttributedString *attString  = text;
//////
////    CGFloat height = [self textViewHeightForAttributedText:attString andWidth:300];
//////    
//////    return height;
////    
////    if ((height + 40) > 60) {
////        return height + 40;
////    }
//
//    
//    if (height > 60) {
//        return height + 44;
//    }
//    return 60;
//}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

//// In a storyboard-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
//{
//
//    // Get the new view controller using [segue destinationViewController].
//    // Pass the selected object to the new view controller.
//}


- (IBAction)addNews:(id)sender {
    
    [self performSegueWithIdentifier:@"addNews" sender:self];
    
//    NSDictionary *tweetData = [NSDictionary dictionaryWithObjectsAndKeys:@"teste news app", @"text", nil];
//    
//    self.newsArray = [webServiceController sendDataToWebServiceWithData:tweetData service:@"tweets" andHTTPMethod:@"POST"];
//    
//    [self.tableView reloadData];
    
}

- (IBAction)userDetails:(id)sender {
    
    [self performSegueWithIdentifier:@"userDetails" sender:self];
}
@end
