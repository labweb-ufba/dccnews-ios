//
//  UserDetailsTableViewController.m
//  DCCNews
//
//  Created by Douglas on 21/07/14.
//  Copyright (c) 2014 Douglas. All rights reserved.
//

#import "UserDetailsTableViewController.h"

@interface UserDetailsTableViewController ()

@end

@implementation UserDetailsTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    webServiceHandler = [[WebServiceHandler alloc] init];
    
    NSArray *tempArray1 = [NSArray arrayWithObjects:@"Minhas Notícias", @"Editar Perfil", nil];
    NSArray *tempArray2 = [NSArray arrayWithObjects:@"Logout", nil];
    
    self.tableData = [NSDictionary dictionaryWithObjectsAndKeys:tempArray1, @"a", tempArray2, @"b", nil];
    
    self.allKeys = [[self.tableData allKeys] sortedArrayUsingSelector:@selector(compare:)];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [self.allKeys count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *sectionArray = [self.tableData objectForKey:[self.allKeys objectAtIndex:section]];
    // Return the number of rows in the section.
    return [sectionArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"userDetailCell" forIndexPath:indexPath];
    
    NSInteger row = [indexPath row];
    NSInteger section = [indexPath section];
    
    NSArray *sectionArray = [self.tableData objectForKey:[self.allKeys objectAtIndex:section]];
    
    cell.textLabel.text = [sectionArray objectAtIndex:row];
    
    if (section == 0) {
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    }
    else{
        [cell.textLabel setTextAlignment:NSTextAlignmentCenter];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([indexPath section] == 1) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Logout" message:@"Deseja Encerrar a Sessão?" delegate:self cancelButtonTitle:@"Não" otherButtonTitles:@"Sim", nil];
        
        alert.delegate = self;
        
        [alert show];
    }
    

    
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == 1) {
        [webServiceHandler sendDataToWebServiceWithData:nil service:@"session" andHTTPMethod:@"DELETE"];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        [defaults setObject:nil forKey:@"userInfo"];
        [defaults synchronize];
        
        //[self.navigationController dismissViewControllerAnimated:NO completion:nil];
        
        [self performSegueWithIdentifier:@"login" sender:self];
    }
    
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cancel:(id)sender {
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
@end
