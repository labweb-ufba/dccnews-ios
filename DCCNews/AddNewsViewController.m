//
//  AddNewsViewController.m
//  DCCNews
//
//  Created by Douglas on 21/07/14.
//  Copyright (c) 2014 Douglas. All rights reserved.
//

#import "AddNewsViewController.h"

@interface AddNewsViewController ()

@end

@implementation AddNewsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    webServiceHandler = [[WebServiceHandler alloc] init];
    
    [self.newsTextView becomeFirstResponder];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cancel:(id)sender {
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)done:(id)sender {
    
    if ([self.newsTextView.text length] > 0 && [self.newsTextView.text length] < 200) {
        NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:self.newsTextView.text, @"text", nil];
        
        NSLog(@"request data %@", requestData);
        
        NSDictionary *tweetData = [webServiceHandler sendDataToWebServiceWithData:requestData service:@"tweets" andHTTPMethod:@"POST"];
        
        if (tweetData) {
            NSLog(@"foi");
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        }
    }
    else{
        if ([self.newsTextView.text length] == 0) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Escreva alguma coisa" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            
            [alert show];
        }
        
        if ([self.newsTextView.text length] > 200) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Número máximo de caracteres é 200" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            
            [alert show];
        }

    }
    
}
@end
