//
//  NewsFeedTableViewController.h
//  DCCNews
//
//  Created by Douglas on 21/07/14.
//  Copyright (c) 2014 Douglas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebServiceHandler.h"

@interface NewsFeedTableViewController : UITableViewController{
    
    WebServiceHandler *webServiceController;
    
}

@property (strong, nonatomic) IBOutlet UIBarButtonItem *userBarButton;
@property(strong, nonatomic) NSArray *newsArray;

@property(strong, nonatomic) NSMutableArray *usersArray;
@property(strong, nonatomic) NSMutableArray *heighsArray;

- (IBAction)addNews:(id)sender;
- (IBAction)userDetails:(id)sender;

@end
