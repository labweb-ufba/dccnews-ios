//
//  LoginViewController.m
//  DCCNews
//
//  Created by Douglas on 21/07/14.
//  Copyright (c) 2014 Douglas. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    webServiceHandler = [[WebServiceHandler alloc] init];
    [self.activityView setHidden:YES];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)login:(id)sender {
    
    NSLog(@"login");
    [self.activityView setHidden:NO];
    [self.activityView startAnimating];
    
    [self.emailTextField resignFirstResponder];
    [self.passTextField resignFirstResponder];
    
    UIButton *button = (UIButton *)sender;
    
    [button setEnabled:NO];
    [self.emailTextField setEnabled:NO];
    [self.passTextField setEnabled:NO];
    
    
    NSDictionary *userData = [NSDictionary dictionaryWithObjectsAndKeys:self.emailTextField.text, @"email", self.passTextField.text, @"password", nil];
    
    NSString *service = @"session";
    
    NSString *method = @"POST";
    
    NSArray *info = [NSArray arrayWithObjects:userData, service, method, nil];
    
    [NSThread detachNewThreadSelector:@selector(performRequestWithInfo:) toTarget:self withObject:info];
}

- (void)performRequestWithInfo:(NSArray *)info{
    
    NSDictionary *userInfo = [webServiceHandler sendDataToWebServiceWithData:[info objectAtIndex:0] service:[info objectAtIndex:1] andHTTPMethod:[info objectAtIndex:2]];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSLog(@"array %@", userInfo);
    
    if ([userInfo objectForKey:@"username"]) {
        
        [defaults setObject:userInfo forKey:@"userInfo"];
        [defaults synchronize];
        
        [self dismissViewControllerAnimated:NO completion:nil];
    }
    
    [self.activityView stopAnimating];
    [self.activityView setHidden:YES];
    [self.emailTextField setEnabled:YES];
    [self.emailTextField setEnabled:YES];
    [self.loginButton setEnabled:YES];
    
}
@end
