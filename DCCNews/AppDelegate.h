//
//  AppDelegate.h
//  DCCNews
//
//  Created by Douglas on 21/07/14.
//  Copyright (c) 2014 Douglas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
