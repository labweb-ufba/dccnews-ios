//
//  WebServiceHandler.h
//  DCCNews
//
//  Created by Douglas on 21/07/14.
//  Copyright (c) 2014 Douglas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WebServiceHandler : NSObject

- (id)sendDataToWebServiceWithData:(NSDictionary*)dataString service:(NSString *)service andHTTPMethod:(NSString *)method;

@end
