//
//  WebServiceHandler.m
//  DCCNews
//
//  Created by Douglas on 21/07/14.
//  Copyright (c) 2014 Douglas. All rights reserved.
//

#import "WebServiceHandler.h"

#define URLHome  @"http://sistema-de-noticias-webservice.herokuapp.com/"

@implementation WebServiceHandler


- (id)sendDataToWebServiceWithData:(NSDictionary*)dataString service:(NSString *)service andHTTPMethod:(NSString *)method{
    
    NSHTTPURLResponse *response = nil;
    NSError *error;
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@", URLHome, service];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    

    [request setHTTPShouldHandleCookies:YES];
    
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:method];
    
    if ([method isEqualToString:@"POST"]) {
        
        NSMutableData *body = [NSMutableData data];
        
        NSArray *allKeys = [dataString allKeys];
        NSString *bodyString = [NSString stringWithFormat:@"service=%@", service];
        
        for (NSString *key in allKeys) {
            bodyString = [NSString stringWithFormat:@"%@=%@&%@", key, [dataString objectForKey:key], bodyString];
        }
        
        //NSLog(@"body %@", bodyString);
        
        [body appendData:[bodyString dataUsingEncoding:NSUTF8StringEncoding]];
        
        [request setHTTPBody:body];

    }
    
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSLog(@"%@", response);
    
    
    if (data){
        id responseMensages = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        //NSLog(@"jason: %@", responseMensages);
        
        return responseMensages;
    }
    
    if(response == nil){
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error01" message:@"Unable to contact server." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        
        
        [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:NO];
    }
    
    return nil;
}

@end
