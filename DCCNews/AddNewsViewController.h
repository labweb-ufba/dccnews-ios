//
//  AddNewsViewController.h
//  DCCNews
//
//  Created by Douglas on 21/07/14.
//  Copyright (c) 2014 Douglas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebServiceHandler.h"

@interface AddNewsViewController : UIViewController{
    
    WebServiceHandler *webServiceHandler;
    
}
@property (strong, nonatomic) IBOutlet UITextView *newsTextView;

- (IBAction)cancel:(id)sender;
- (IBAction)done:(id)sender;

@end
