//
//  LoginViewController.h
//  DCCNews
//
//  Created by Douglas on 21/07/14.
//  Copyright (c) 2014 Douglas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebServiceHandler.h"

@interface LoginViewController : UIViewController{
    
    WebServiceHandler *webServiceHandler;
    
    
}
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UITextField *passTextField;
@property (strong, nonatomic) IBOutlet UIButton *loginButton;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityView;

- (IBAction)login:(id)sender;

@end
