//
//  UserDetailsTableViewController.h
//  DCCNews
//
//  Created by Douglas on 21/07/14.
//  Copyright (c) 2014 Douglas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebServiceHandler.h"


@interface UserDetailsTableViewController : UITableViewController<UIAlertViewDelegate>{
    
    WebServiceHandler *webServiceHandler;
    
    
    
}

@property(strong, nonatomic) NSDictionary *tableData;
@property(strong, nonatomic) NSArray *allKeys;

- (IBAction)cancel:(id)sender;

@end
